﻿using System;
using System.Collections.Generic;

namespace Mic.Homework.Phonebook
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var phonebook = new PhoneBook();
            var dic = phonebook.CreateContactBook(3);
            phonebook.PrintDictionary(dic);
            Console.WriteLine();
            do
                phonebook.SerchNumber(dic);
            while (Console.ReadKey().Key != ConsoleKey.Escape);
            phonebook.Delete(dic, "098825308");
            Console.ReadLine();
        }

    }
}
