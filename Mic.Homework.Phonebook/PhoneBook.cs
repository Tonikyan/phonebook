﻿using System;
using System.Collections.Generic;
namespace Mic.Homework.Phonebook
{
    class PhoneBook
    {
        public void SerchNumber(Dictionary<string, Contact> dic)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Write PhooneNumber ");
            string number;
            do
            {
                number = Console.ReadLine();
                if (number.Length == 12 && number[0] == '+')
                    number = number.Substring(4, number.Length -4);
                if (!(number.Length == 9 || number.Length == 8))
                { 
                Console.Write("Number length Is not it right: ");
                }
            }
            while(!(number.Length == 9 || number.Length == 8));
            if (dic.ContainsKey(number))
            {
                Print(dic, number);
            }
            else
            {
                if (number.Length == 9 || number[0] == '0')
                    number = number.TrimStart('0');
                Console.WriteLine("if you want to add a contact press enter");
                if (Console.ReadKey().Key == ConsoleKey.Enter)
                {
                    Console.Write("Write your Name: ");
                    var name = Console.ReadLine();
                    Console.Write("Write your Surename: ");
                    var surName = Console.ReadLine();
                    Console.Write("Write your Email");
                    var email = Console.ReadLine();
                    var cont = CreateContact(number, name, surName,email);
                    dic.Add(cont.PhoneNumber, cont);
                    Console.WriteLine("your contact has been added");
                    number = "+374" + number;
                    Print(dic, number);
                }
            }
            Console.WriteLine(" For exit press esc");
            Console.ResetColor();
        }

        public void Delete(Dictionary<string, Contact> dic,string number)
        {
            dic.Remove(number);
        }

        private Contact CreateContact(string number, string name, string surName,string email)
        {
            Contact cont = new Contact()
            {
                _phoneNumber = number,
                Name = name,
                SureName = surName,
                Email = email
            };
            var ph = cont.NumCode;
            if (ph == "93" || ph == "94" || ph == "77" || ph == "98" || ph == "49")
            {
                cont.Firm = "Vivacell";
            }
            else if (ph == "91" || ph == "96" || ph == "99" || ph == "43")
            {
                cont.Firm = "Beeline";
            }
            else if (ph == "95" || ph == "55" || ph == "49")
            {
                cont.Firm = "Ucom";
            }
            return cont;
        }

        public Dictionary<string, Contact> CreateContactBook(int n)
        {
            var phoneBook = new Dictionary<string, Contact>(n);
            var rd = new Random();
            string[] phoneNumCods = { "93", "94", "77", "98", "49", "91", "96", "99", "43", "55", "95", "49" };
            string[] emails = { "@gmail.com", "@yahoo.com", "@mail.ru" };
            for (int i = 0; i < n; i++)
            {
                var name = $"A{i}";
                var surName = $"A{i}yan";
                var mail = surName + emails[rd.Next(emails.Length)];
                var number = phoneNumCods[rd.Next(phoneNumCods.Length)] + rd.Next(100000,1000000);
                var cont = CreateContact(number, name, surName,mail);
                phoneBook.Add(cont.FullNumber, cont);
            }
            return phoneBook;
        }

        private void Print(Dictionary<string, Contact> dic, string number)
        {
            var contact = dic[number];
            var firm = contact.Firm;
            if (firm == "Vivacell")
                Console.ForegroundColor = ConsoleColor.Red;
            else if (firm == "Beeline")
                Console.ForegroundColor = ConsoleColor.Yellow;
            else if (firm == "Ucom")
                Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(contact.FullName);
            Console.WriteLine(contact.PhoneNumberFormat);
            Console.WriteLine(contact.Firm);
            Console.WriteLine(contact.Email);
        }

        public void PrintDictionary(Dictionary<string,Contact> dic)
        {
            foreach (var item in dic.Values)
            {
                Print(dic, item.FullNumber);
            }
        }
    }
}
