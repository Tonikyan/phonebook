﻿using System;
using System.Text;

namespace Mic.Homework.Phonebook
{
    class Contact
    {
        public string Firm { get; set; }
        public string Name { get; set; }
        public string SureName { get; set; }
        public string FullName => $"{Name} {SureName}";
        public string _phoneNumber;
        public string PhoneNumber {
            get
            {
                if (_phoneNumber[0] == '0' || _phoneNumber.Length == 9)
                    return "+374" + _phoneNumber.TrimStart('0');
                else
                    return"+374" + _phoneNumber;
            }
            set
            {
                value = _phoneNumber;
            }
        }
        public string FullNumber =>PhoneNumber;
        public string NumCode => GetCode();
        public string PhoneNumberFormat => "+374" + String.Format("{0: ## ##-##-##}", Convert.ToInt64(PhoneNumber));
        public string Email { get; set; }
        public string GetCode()
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 4;i<6; i++)
                sb.Append(FullNumber[i]);
            return Convert.ToString(sb);
        }
    }
}
